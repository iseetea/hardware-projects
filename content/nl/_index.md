---
title: "Turing Lab Hardware Projects"

description: "Student-made Hardware Know-How"
cascade:
  featured_image: '/images/hl15.jpg'
---

# Welkom!

Welkom op het hardware projecten blog van het HU HBO-ICT Turing Lab! Hieronder zie je de meest recente toevoegingen. Kijk [hier](about) voor een toelichting op het concept van deze blog, of [hier](post) voor de hele lijst met projecten.

Deze site is er voor en door alle gebruikers van het HU HBO-ICT Turing Lab en de daarbijbehorende [labshop](https://hu-hbo-ict.gitlab.io/turing-lab/ti-lab-shop/). Hier hebben we een steeds groeiend en soms wisselend assortiment aan onderdelen en modules, maar buiten de gelinkte datasheets missen we soms nog wat praktische voorbeelden. Deze site is een initiatief om daar iets aan te veranderen. [Hoe dan?](/about)
