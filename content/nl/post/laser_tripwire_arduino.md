---
title: "Arduino Laser Tripwires"
description: "Ontvanger voor een laser-tripwire, Arduino based"
author: "Brian"
date: 2022-09-01T11:28:08+02:00
featured_image: "/images/tripwire/wiring_inc_buzzer.jpg"
tags: ["Laser", "Game", "Arduino", "GL12527", "BC550", "Active Buzzer"]
---

Deze versie gebruikt een Arduino en een mini-breadboard (via een proto-shield) om de componenten met elkaar te verbinden.

## Benodigdheden
- Arduino
- Shield met breadboard
- LDR
- Weerstand (1KΩ)
- Active Buzzer

## Code
```c
int ldrPin = A0;
int buzzPin = 13;
bool buzz = false;
int value = 0;      // stores the analog LDR reading
int threshold = 0;  // value increases as light decreases
                    //   experiment to find best value for your hardware

void setup()
{ pinMode(buzzPin, OUTPUT);
  digitalWrite(buzzPin, LOW); }

void loop()
{ value = analogRead(ldrPin);

  if (buzz && value > threshold) // if too dark for two 0.5s intervals
  { digitalWrite(buzzPin, HIGH); }
  else
  { digitalWrite(buzzPin, LOW); }

  if (value > threshold) // save state for next iteration
  { buzz = true; }
  else
  { buzz = false; }

  delay(500); }
```

## Aansluiting
![](/images/tripwire/wiring_inc_buzzer.jpg)
![](/images/tripwire/wiring_large_ldr.jpg)
